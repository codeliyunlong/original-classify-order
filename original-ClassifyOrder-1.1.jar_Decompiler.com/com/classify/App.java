package com.classify;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class App {
   private static String filterOrderPath = "";
   private static final Map<String, Set<String>> uniqueStatistics = new HashMap();
   private static final Map<String, Map<String, Integer>> statisticsNum = new HashMap();
   private static final Map<String, Map<String, Map<String, Set<String>>>> statisticsAttributes = new HashMap();
   private static final String OBJECT = "object";
   private static final String MAPPING = "mapping";
   private static final String DELETE = "DELETE";
   private static final String REGIST = "REGIST";
   private static final String UPDATE = "UPDATE";

   public static void main(String[] args) {
      System.out.println("程序开始执行...");
      long start = System.currentTimeMillis();
      if (args.length != 2) {
         System.err.println("启动参数有误，请检查后重新启动程序");
      } else {
         String rawOrderPath = args[0];
         filterOrderPath = args[1] + "\\";
         System.out.println("待分类的工单目录路径：" + rawOrderPath);
         System.out.println("分类后的工单存放的路径：" + filterOrderPath);
         traverseFolders(FileUtil.ls(rawOrderPath));
         outputStatisticalResults();
         System.out.println("程序执行结束，耗时：" + (System.currentTimeMillis() - start) + "ms");
      }
   }

   private static void outputStatisticalResults() {
      try {
         FileWriter writer1 = new FileWriter(filterOrderPath + "工单对象的全部字段.json");

         try {
            FileWriter writer2 = new FileWriter(filterOrderPath + "工单类型数量统计.txt");

            try {
               writer1.write(JSONUtil.toJsonStr((Object)statisticsAttributes));
               int sum = 0;
               Iterator var3 = statisticsNum.keySet().iterator();

               while(true) {
                  if (!var3.hasNext()) {
                     writer2.append("全部合计数量为：").append(String.valueOf(sum));
                     break;
                  }

                  String k1 = (String)var3.next();
                  writer2.append(k1).append("\n");
                  Map<String, Integer> map = (Map)statisticsNum.get(k1);
                  int typeNum = 0;

                  String k2;
                  for(Iterator var7 = map.keySet().iterator(); var7.hasNext(); typeNum += (Integer)map.get(k2)) {
                     k2 = (String)var7.next();
                     writer2.append("    ").append(k2).append(":").append(String.valueOf(map.get(k2))).append("\n");
                  }

                  writer2.append("    ").append(k1).append("类型的总数为：").append(String.valueOf(typeNum)).append("\n");
                  sum += typeNum;
               }
            } catch (Throwable var11) {
               try {
                  writer2.close();
               } catch (Throwable var10) {
                  var11.addSuppressed(var10);
               }

               throw var11;
            }

            writer2.close();
         } catch (Throwable var12) {
            try {
               writer1.close();
            } catch (Throwable var9) {
               var12.addSuppressed(var9);
            }

            throw var12;
         }

         writer1.close();
      } catch (IOException var13) {
         throw new RuntimeException(var13);
      }
   }

   public static void traverseFolders(File[] files) {
      File[] var1 = files;
      int var2 = files.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         File file = var1[var3];
         if (file.isDirectory()) {
            File[] files1 = file.listFiles();
            if (files1 != null) {
               traverseFolders(files1);
            }
         } else {
            processXML(file);
         }
      }

   }

   public static void processXML(File file) {
      if (!ObjUtil.isEmpty(file)) {
         SAXReader saxReader = new SAXReader();

         Document document;
         try {
            document = saxReader.read(file.getPath());
         } catch (Exception var20) {
            System.err.println("读取文件异常:" + file.getPath());
            return;
         }

         Element rootElement = document.getRootElement();
         Element objects = rootElement.element("Objects");
         Element mappings = rootElement.element("Mappings");
         String action = "OTHER";
         boolean updateAction = false;
         HashSet<String> objectsAttributes = new HashSet();
         HashSet<String> mappingsAttributes = new HashSet();
         StringBuilder logotype = new StringBuilder();
         Iterator var11;
         Element element;
         String md5Logotype;
         String elementType;
         if (ObjUtil.isNotNull(objects)) {
            var11 = objects.elements().iterator();

            while(var11.hasNext()) {
               element = (Element)var11.next();
               md5Logotype = element.attributeValue("Action");
               elementType = element.attributeValue("ElementType");
               if (!updateAction) {
                  action = md5Logotype;
                  updateAction = true;
               }

               objectsAttributes.add(elementType);
               logotype.append(md5Logotype).append(elementType);
               HashSet<String> set = new HashSet();
               Iterator var16 = element.elements().iterator();

               while(var16.hasNext()) {
                  Element e = (Element)var16.next();
                  String name = e.attributeValue("Name");
                  set.add(name);
                  putStatisticsAttributes(md5Logotype, "object", elementType, name);
               }

               logotype.append(StrUtil.join("", set));
            }
         }

         if (ObjUtil.isNotNull(mappings)) {
            var11 = mappings.elements().iterator();

            while(var11.hasNext()) {
               element = (Element)var11.next();
               md5Logotype = element.attributeValue("Action");
               elementType = element.attributeValue("ElementType");
               String parentType = element.attributeValue("ParentType");
               if (!updateAction) {
                  action = md5Logotype;
                  updateAction = true;
               }

               mappingsAttributes.add(elementType + "@" + parentType);
               logotype.append(md5Logotype).append(elementType).append("@").append(parentType);
               HashSet<String> set = new HashSet();
               Iterator var29 = element.elements().iterator();

               while(var29.hasNext()) {
                  Element e = (Element)var29.next();
                  String name = e.attributeValue("Name");
                  set.add(name);
                  putStatisticsAttributes(md5Logotype, "mapping", elementType + "&" + parentType, name);
               }

               logotype.append(StrUtil.join("", set));
            }
         }

         StringBuilder structureName = new StringBuilder();
         structureName.append("Objects");
         Iterator var24 = objectsAttributes.iterator();

         while(var24.hasNext()) {
            md5Logotype = (String)var24.next();
            structureName.append("-").append(md5Logotype);
         }

         structureName.append("-").append("Mappings");
         var24 = mappingsAttributes.iterator();

         while(var24.hasNext()) {
            md5Logotype = (String)var24.next();
            structureName.append("-").append(md5Logotype);
         }

         String md5Structure = SecureUtil.md5(structureName.toString());
         md5Logotype = SecureUtil.md5(logotype.toString());

         try {
            if (!uniqueStatistics.containsKey(md5Structure)) {
               uniqueStatistics.put(md5Structure, new HashSet());
            }

            if (((Set)uniqueStatistics.get(md5Structure)).add(md5Logotype)) {
               System.out.println("正在复制" + file.getPath());
               Map map = (Map)statisticsNum.get(action);

               try {
                  map.put(structureName.toString(), (Integer)map.getOrDefault(structureName.toString(), 0) + 1);
               } catch (Exception var21) {
                  if (!statisticsNum.containsKey(action)) {
                     statisticsNum.put(action, new HashMap());
                  }

                  map = (Map)statisticsNum.get(action);
                  map.put(structureName.toString(), (Integer)map.getOrDefault(structureName.toString(), 0) + 1);
               }

               FileUtil.copy(file, new File(filterOrderPath + action + "\\" + structureName + "\\" + file.getName()), true);
            }
         } catch (IORuntimeException var22) {
            System.err.println("复制文件出现异常," + filterOrderPath + action + "\\" + structureName + "\\" + file.getName());
         }

      }
   }

   private static void putStatisticsAttributes(String action, String type, String elementType, String attrName) {
      Map map;
      try {
         map = (Map)((Map)statisticsAttributes.get(action)).get(type);
         if (map == null) {
            throw new RuntimeException();
         }
      } catch (Exception var6) {
         initStatisticsAttributes(action, type);
         map = (Map)((Map)statisticsAttributes.get(action)).get(type);
      }

      if (!map.containsKey(elementType)) {
         map.put(elementType, new HashSet());
      }

      ((Set)map.get(elementType)).add(attrName);
   }

   private static void initStatisticsAttributes(String action, String type) {
      if (!statisticsAttributes.containsKey(action)) {
         statisticsAttributes.put(action, new HashMap());
      }

      Map<String, Map<String, Set<String>>> map = (Map)statisticsAttributes.get(action);
      if (!map.containsKey(type)) {
         map.put(type, new HashMap());
      }

   }

   static {
      statisticsNum.put("DELETE", new HashMap());
      statisticsNum.put("REGIST", new HashMap());
      statisticsNum.put("UPDATE", new HashMap());
      statisticsAttributes.put("DELETE", new HashMap());
      statisticsAttributes.put("REGIST", new HashMap());
      statisticsAttributes.put("UPDATE", new HashMap());
      ((Map)statisticsAttributes.get("DELETE")).put("object", new HashMap());
      ((Map)statisticsAttributes.get("DELETE")).put("mapping", new HashMap());
      ((Map)statisticsAttributes.get("REGIST")).put("object", new HashMap());
      ((Map)statisticsAttributes.get("REGIST")).put("mapping", new HashMap());
      ((Map)statisticsAttributes.get("UPDATE")).put("object", new HashMap());
      ((Map)statisticsAttributes.get("UPDATE")).put("mapping", new HashMap());
   }
}
