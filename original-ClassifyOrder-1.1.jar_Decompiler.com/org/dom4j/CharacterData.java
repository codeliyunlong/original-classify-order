package org.dom4j;

public interface CharacterData extends Node {
   void appendText(String var1);
}
